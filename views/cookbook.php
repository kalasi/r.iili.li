<!DOCTYPE html>
<html>
  <body>
    <p>
      <a href='http://iili.li'>iili home</a> - <a href='/'>recipes</a>
    </p>
    <h1>
      <?= $name; ?>
    </h1>
    <ul>
<?php foreach ($recipes as $recipe): ?>
      <li>
        <a href='/<?= $cookbookEncoded; ?>/<?= $recipe['encoded']; ?>'><?= $recipe['name']; ?></a>
      </li>
<?php endforeach; ?>
  </body>
</html>
