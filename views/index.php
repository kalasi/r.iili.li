<!DOCTYPE html>
<html>
  <body>
    <p>
      <a href='http://iili.li'>iili home</a>
    </p>
    <h1>
      Recipes
    </h1>
    <p>
      Be sure to bookmark your cookbooks!
    </p>
    <form action='create' method='POST'>
      <label for='name'>
        Name:
      </label>
      <input type='text' id='name' name='name' required>
      <br>
      <label for='cookbook'>
        Cookbook Name:
      </label>
      <input type='text' id='cookbook' name='cookbook' required>
      <br>
      <label for='password'>
        Recipe Password (can be empty):
      </label>
      <input type='password' id='password' name='password'>
      <br>
      <label for='recipe'>
        Recipe:
      </label>
      <br>
      <textarea id='recipe' name='recipe' style='width: 50%; height: 300px;' required></textarea>
      <p>
        Use <a href='https://help.github.com/articles/markdown-basics/'>Markdown</a> for formatting.
      </p>
      <button type='submit'>
        Create Recipe
      </button>
    </form>
  </body>
</html>
