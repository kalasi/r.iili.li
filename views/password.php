<!DOCTYPE html>
<html>
  <body>
    <p>
      <a href='http://iili.li'>iili home</a> - <a href='/'>recipes</a> - <a href='/<?= $cookbookEncoded; ?>'><?= $cookbook; ?></a>
    </p>
    <p>
      A password is required to view this recipe.
    </p>
    <br>
    <form method='POST'>
      <label for='password'>
        Password:
      </label>
      <input type='password' id='password' name='password' required>
      <br>
      <button type='submit'>
        Submit
      </button>
    </form>
  </body>
</html>
