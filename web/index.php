<?php

define('STORAGE', __DIR__ . './../storage/');

require __DIR__ . './../vendor/autoload.php';

use League\CommonMark\CommonMarkConverter;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Application;

$app->get('/', function () {
    require __DIR__ . './../views/index.php';

    return '';
});

$app->post('create', function (Request $request) use ($app) {
    $name = $request->get('name');
    $cookbook = $request->get('cookbook');
    $password = $request->get('password');
    $recipe = $request->get('recipe');
    $encodedName = urlencode($name);
    $encodedCookbook = urlencode($cookbook);

    if (empty($name) || empty($cookbook) || empty($recipe)) {
        require __DIR__ . './../views/validation.php';

        return '';
    }

    $converted = (new CommonMarkConverter)->convertToHtml($recipe);

    $cookbooks = unserialize(file_get_contents(STORAGE . 'cookbooks'));

    // Check if the cookbooks file existed.
    if ($cookbooks === false) {
        $cookbooks = [];
    }

    // Add this cookbook if it doesn't exist.
    if (!isset($cookbooks[$cookbook])) {
        $cookbooks[$cookbook] = [];
    }

    // Make the folder if it doesn't exist.
    mkdir(STORAGE . $encodedCookbook, 755, true);

    // Get this cookbook's information.
    $cookbookData = unserialize(file_get_contents(STORAGE . $encodedCookbook . '/recipes'));

    // Check if the recipes file existed.
    if ($cookbookData === false) {
        $cookbookData = [];
    }

    // Check if this name is in the cookbook already.
    if (isset($cookbookData[$name])) {
        require __DIR__ . './../views/in_use.php';

        return '';
    }

    // Add this recipe name to the cookbooks.
    $cookbooks[$cookbook][$name] = [
        'password' => $password,
    ];

    // Recipe data.
    $cookbookData[$name] = [
        'password' => $password,
    ];

    // Now store the files.
    file_put_contents(STORAGE . $encodedCookbook . '/' . $encodedName . '.recipe', $converted);
    file_put_contents(STORAGE . 'cookbooks', serialize($cookbooks));
    file_put_contents(STORAGE . $encodedCookbook . '/recipes', serialize($cookbookData));

    return $app->redirect('/' . $encodedCookbook . '/' . $encodedName);
});

// View a cookbook.
$app->get('{cookbook}', function ($cookbook) {
    $cookbook = urldecode($cookbook);
    $cookbookEncoded = urlencode($cookbook);

    $recipeList = unserialize(file_get_contents(STORAGE . $cookbookEncoded . '/recipes'));

    $recipes = [];

    foreach ($recipeList as $recipeName => $recipe) {
        array_push($recipes, [
            'name' => $recipeName,
            'encoded' => urlencode($recipeName),
        ]);
    }

    require __DIR__ . './../views/cookbook.php';

    return '';
});

// View a recipe.
$app->get('{cookbook}/{name}', function ($cookbook, $name) {
    $name = urldecode($name);
    $cookbook = urldecode($cookbook);
    $cookbookEncoded = urlencode($cookbook);
    $nameEncoded = urlencode($name);

    $recipes = unserialize(file_get_contents(STORAGE . $cookbookEncoded . '/recipes'));

    if (!isset($recipes[$name])) {
        require __DIR__ . './../views/404.php';

        return '';
    }

    if (strlen($recipes[$name]['password']) > 0) {
        require __DIR__ . './../views/password.php';

        return '';
    }

    $contents = file_get_contents(STORAGE . $cookbookEncoded . '/' . $nameEncoded . '.recipe');

    require __DIR__ . './../views/recipe.php';

    return '';
});

// View a recipe with a password.
$app->post('{cookbook}/{name}', function ($cookbook, $name, Request $request) {
    $name = urldecode($name);
    $cookbook = urldecode($cookbook);
    $cookbookEncoded = urlencode($cookbook);
    $nameEncoded = urlencode($name);
    $password = $request->get('password');

    $recipes = unserialize(file_get_contents(STORAGE . $cookbookEncoded . '/recipes'));

    if (!isset($recipes[$name])) {
        require __DIR__ . './../views/404.php';

        return '';
    }

    if ($recipes[$name]['password'] !== $password) {
        require __DIR__ . './../views/no_match.php';

        return '';
    }

    $contents = file_get_contents(STORAGE . $cookbookEncoded . '/' . $nameEncoded . '.recipe');

    require __DIR__ . './../views/recipe.php';

    return '';
});

$app->run();
